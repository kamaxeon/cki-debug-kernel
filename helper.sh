#!/bin/bash
    # Export general pipeline functions
    # GENERAL-BEGIN do not modify this line

    # Set an rc parameter.
    # Args: section name value
    function rc_set() {
      "${VENV_PY3}" -m cki.cki_tools.rc_edit "$RC_FILE" "${FUNCNAME[0]}" "$1" "$2" "$3"
    }

    # Get an rc parameter.
    # Args: section name
    # Output: value
    function rc_get() {
      "${VENV_PY3}" -m cki.cki_tools.rc_edit "$RC_FILE" "${FUNCNAME[0]}" "$1" "$2"
    }

    # Delete an rc parameter.
    # Args: section name
    function rc_del() {
      "${VENV_PY3}" -m cki.cki_tools.rc_edit "$RC_FILE" "${FUNCNAME[0]}" "$1" "$2"
    }

    # Set an rc state parameter.
    # Args: name value
    function rc_state_set() {
      "${VENV_PY3}" -m cki.cki_tools.rc_edit "$RC_FILE" "${FUNCNAME[0]}" "$1" "$2"
    }

    # Get an rc state parameter.
    # Args: name
    # Output: value
    function rc_state_get() {
      "${VENV_PY3}" -m cki.cki_tools.rc_edit "$RC_FILE" "${FUNCNAME[0]}" "$1"
    }

    # Delete an rc state parameter.
    # Args: name
    function rc_state_del() {
      "${VENV_PY3}" -m cki.cki_tools.rc_edit "$RC_FILE" "${FUNCNAME[0]}" "$1"
    }

    # Write a bold green message (that looks like gitlab-runner's messages).
    # Args:
    #  $1: Message to print in green
    function echo_green {
        echo -e "\e[1;32m${1}\e[0m"
    }

    # Write a bold red message (that looks like gitlab-runner's messages).
    # Args:
    #  $1: Message to print in red
    function echo_red {
        echo -e "\e[1;31m${1}\e[0m"
    }

    # Write a bold yellow message (that looks like gitlab-runner's messages).
    # Args:
    #  $1: Message to print in red
    function echo_yellow {
        echo -e "\e[1;33m${1}\e[0m"
    }

    function git_clean_url() {
        local repo_url=$1
        repo_url=${repo_url#*//}
        repo_url=${repo_url%/}
        repo_url=${repo_url%.git}
        echo "https://${repo_url}.git/"
    }

    # Retry a command MAX_TRIES times, with MAX_WAIT seconds in between.
    # Args:
    #   $@: command to run
    function loop {
        # shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
        loop_custom $MAX_TRIES $MAX_WAIT "$@"
    }

    # Retry a command multiple times, with an adjustable in between.
    # Args:
    #   $1: number of times to repeat command
    #   $2: delay between runs
    #   $@: command to run
    function loop_custom {
        local count=$1 delay=$2 s
        shift 2
        # shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
        for _ in $(seq $count); do
            # we want set -e still in effect, but also get the exit code; so
            # use a background process, as set -e is kept for background
            # commands, and the exit code checking is possible with wait later
            "$@" &
            wait $! && s=0 && break || s=$? && sleep $delay
        done
        (exit $s)
    }

    # Check if the passed variable has a truthy value or not.
    # Args: Variable
    # Returns: 0 if the variable is truthy, 1 otherwise
    function is_true {
        if [[ "${1}" = [Tt]rue ]] ; then
            return 0
        else
            return 1
        fi
    }

    function _git_cache_clone {
        aws_s3_download BUCKET_GIT_CACHE "${owner_repo}.tar" - | \
            tar -xf - -C "${GIT_CACHE_DIR}/${owner_repo}"
    }

    # Clone a repository from the git-cache.
    # Args:
    #   $1: complete repository URL
    #   $@: additional arguments for git clone, e.g. working directory
    function git_cache_clone {
        local owner_repo
        owner_repo="$(get_owner_repo "$1")"
        rm -rf "${GIT_CACHE_DIR:?}/${owner_repo}"
        mkdir -p "${GIT_CACHE_DIR}/${owner_repo}"
        loop _git_cache_clone
        shift
        git clone --quiet "${GIT_CACHE_DIR}/${owner_repo}" "$@"
        rm -rf "${GIT_CACHE_DIR:?}/${owner_repo}"
    }

    function download_software {
      # Download the prepared software object and untar it into ${SOFTWARE_DIR}.
      function get_software_object {
          aws_s3_download BUCKET_SOFTWARE "$SOFTWARE_OBJECT" - | xz -d - | \
              tar --no-same-owner --no-same-permissions --no-xattrs --touch -xf - -C "${SOFTWARE_DIR}" --strip-components 1
      }

      echo_green "📥 Downloading prepared CKI software."
      for _ in {1..5}; do
          mkdir -p "${SOFTWARE_DIR}"
          get_software_object && s=0 || s=$?
          if [ $s -eq 0 ]; then
              return
          else
              # Delete a partial restore if there was a network problem during
              # download.
              rm -rf "${SOFTWARE_DIR}"
          fi
      done
      echo_red "Failed to download prepared CKI software."
      return 1
    }

    # Determine the total number of CPUs available.
    #
    # NOTE(mhayden): This process must be done carefully since `nproc` in a
    # container may say that 64 CPUs are available, but cgroups have limited the
    # CPU count to 4. Tools like make, rpm, and xz are not able to determine
    # cgroup limits, so they will use the CPU count from the system instead. This
    # causes reduced performance and often causes OOM errors.
    function get_cfs_quota {
        local QUOTA_FILE="/sys/fs/cgroup/cpu/cpu.cfs_quota_us"
        if [ -f "${QUOTA_FILE}" ]; then
            cat "${QUOTA_FILE}"
        fi
    }

    function get_cpu_count {
        local CFS_QUOTA CPUS_AVAILABLE MAKE_JOBS RPM_BUILD_NCPUS
        CFS_QUOTA="$(get_cfs_quota)"
        if [ -n "${CFS_QUOTA}" ] && [ "${CFS_QUOTA}" != "-1" ]; then
            CPUS_AVAILABLE="$((CFS_QUOTA / 100 / 1000))"
        else
            CPUS_AVAILABLE="$(nproc)"
        fi

        # On the off chance that the CPU count is less than 1, set the count to 1.
        if [ "${CPUS_AVAILABLE}" -lt 1 ]; then
            CPUS_AVAILABLE=1
        fi

        # Set the number of make jobs based on kernel developer recommendations.
        export MAKE_JOBS="$((CPUS_AVAILABLE * 3 / 2))"

        # RPM needs a special environment variable set so that this command returns
        # the correct number of CPUs in the container: rpm --eval %{_smp_mflags} If
        # this is not set, too many XZ_THREADS will be spawned and we will get OOM
        # errors. See FASTMOVING-1526.
        export RPM_BUILD_NCPUS="${MAKE_JOBS}"

        echo 'export CPUS_AVAILABLE="'"${CPUS_AVAILABLE}"'"'
        echo 'export MAKE_JOBS="'"${MAKE_JOBS}"'"'
        echo 'export RPM_BUILD_NCPUS="'"${RPM_BUILD_NCPUS}"'"'
    }

    # Write a stylized header, but fall back to plain text
    # Args:
    #  $@: Message to print
    function say {
        echo "$@" | toilet -f mono12 -w 300 | lolcat -f || echo "$@"
    }

    # Use this to execute an application and log the invocation by prepending
    # this to an existing command.
    function run_logged() {
        bash -x -c '"$@"' -- "$@"
    }

    # Generate -v key=value arguments for kpet from the trigger variables given
    # in KPET_*VARIABLES in the kpet_variable_arguments array.
    function kpet_generate_variable_arguments() {
        kpet_variable_arguments=()
        for kpet_variable in $KPET_VARIABLES $KPET_ADDITIONAL_VARIABLES; do
            if [ -v "$kpet_variable" ]; then
                kpet_variable_arguments+=("-v" "${kpet_variable}=${!kpet_variable}")
            fi
        done
    }

    # Functions for calculating build time durations
    #
    # Store time before build start
    function store_time() {
        BUILD_TIME_START=$(date +"%s")
    }

    # Calculate build time and store into rc
    # Args: rc_variable
    function calculate_duration() {
        echo $(($(date +"%s") - BUILD_TIME_START))
    }

    # Print time in ISO8601 UTC format 2000-21-31T01:23:45.6789Z needed by KCIDB
    function date_utc() {
        date --utc +"%FT%T.%NZ"
    }

    # Join an array using a delimiter.
    function join_by() {
        local IFS="$1"
        shift
        echo "$*"
    }

    function upt_release_testing_resources() {
      local ARG_PY3=$1
      local ARG_UPT_OUTPUT=$2

      "${ARG_PY3}" -m upt cancel -r "${ARG_UPT_OUTPUT}" || true
    }

    # Join an array with a multi-character delimiter, but ensure that
    # delimiter is added to the first item in the array as well. This is
    # helpful for adding '--with=' and '--without' to rpmbuild commands.
    function join_by_multi() {
        local d=$1
        shift
        printf "%s" "${@/#/$d}"
    }

    # Get a modified git_url if authenticated access to a GitLab repo is needed.
    # Original URL is returned if no auth is set up.
    # Args:
    #   $1: git_url variable
    # Env variables:
    #   $GITLAB_READ_REPO_TOKENS: json {"host": "token_name"}
    #   Any variable specified by "token_name"
    function get_auth_git_url {
        local original_url="$1"
        proto="${original_url%%://*}"          # Save protocol
        git_host="${original_url#*//}"         # Strip protocol
        git_host="${git_host%%/*}"             # Save host; strip everything after hostname
        git_repo_path="${original_url#*//*/}"  # Strip protocol and host

        token_name="$(jq --arg GIT_HOST "$git_host" -r '.[$GIT_HOST] // empty' <<< "${GITLAB_READ_REPO_TOKENS:-}")"
        if [ -v "${token_name}" ] ; then
            echo "${proto}://oauth2:${!token_name}@${git_host}/${git_repo_path}"
        else
            echo "$original_url"
        fi
    }

    # Get the short tag for a commit in a repository.
    # Args:
    #   $1: path to the git repository
    function get_short_tag {

        # Does the repo directory exist?
        if [[ ! -d $1 ]]; then
            exit 1
        fi

        # Switch to the repo directory.
        cd "$1"

        # Describe the most recent commit.
        TAG=$(git describe --abbrev=0 || true)

        if [[ $TAG =~ ^kernel ]]; then
            # Handle kernel tags in kernel-VERSION-RELEASE format.
            # shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
            SHORT_TAG=$(echo ${TAG} | cut -d '-' -f 3-)
        elif [[ $TAG =~ ^[0-9] ]]; then
            # Handle kernel tags in VERSION-RELEASE format.
            # shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
            SHORT_TAG=$(echo ${TAG} | cut -d '-' -f 2-)
        else
            # Handle upstream kernel tags, such as v5.0-rc8, or situations where
            # there are no tags at all.
            SHORT_TAG=$(git rev-list --max-count=1 HEAD | cut -b-7)
        fi

        # Return the short tag we generated with a dash prepended.
        echo "-${SHORT_TAG}"

        cd "${OLDPWD}"
    }

    # Get the owner.repo name that is used in the git-cache for a repository.
    #
    # The owner can be overridden by setting GIT_URL_OWNER.
    #
    # Args:
    #   $1: url to the git repository
    function get_owner_repo {
        GIT_URL="$1"
        GIT_URL_LC="${GIT_URL,,}"                 # everything lowercase
        GIT_URL_PATH="${GIT_URL_LC#*//*/}"        # strip protocol and host
        GIT_URL_PATH="${GIT_URL_PATH%/}"          # strip trailing slashes
        GIT_URL_PATH="${GIT_URL_PATH%.git}"       # strip optional .git suffix
        GIT_URL_REPO="${GIT_URL_PATH##*/}"        # strip any directories
        if [ -n "${GIT_URL_OWNER:-}" ]; then
            :                                     # set outside of the function
        elif [[ "${GIT_URL_PATH}" == */* ]]; then
            GIT_URL_OWNER="${GIT_URL_PATH%/*}"    # strip repo
            GIT_URL_OWNER="${GIT_URL_OWNER##*/}"  # strip any parent directories
        else
            # default to "git" owner
            # this makes the following URLs compatible (cgit):
            # - git://host.com/repo.git
            # - http://host.com/git/repo.git
            GIT_URL_OWNER="git"
        fi
        echo "${GIT_URL_OWNER}.${GIT_URL_REPO}"
    }

    # Run skt --help to see if skt works.
    function dryrun_by_skt {
        local ARG_PY3=$1
        local ARG_BKR=$2
        local ARG_BKR_INPUT=$3
        # Verify that skt works properly even when doing a dry run.
        "${ARG_PY3}" -m skt.executable --help | head -n1

        # Check the job
        "${ARG_BKR}" job-submit --dry-run "${ARG_BKR_INPUT}"
        echo "SKT: Dry run was successful. Check the artifacts for beaker.xml to verify that the XML contents are correct."
    }

    # Run upt legacy convert and rcclient --help to see if UPT and restraint client works.
    function dryrun_by_upt {
        local ARG_PY3=$1
        local ARG_UPT_INPUT=$2
        local ARG_UPT_OUTPUT=$3

        # Verify that we can convert beaker.xml
        "${ARG_PY3}" -m upt legacy convert -i "${ARG_UPT_INPUT}" -r "${ARG_UPT_OUTPUT}"
        # Verify that we can run restraint_wrap module.
        "${ARG_PY3}" -m restraint_wrap --help | head -n1
        echo "UPT: Dry run was successful. Check the artifacts for c_req.yaml to verify that the contents are correct."
    }

    # Run kernel testing using skt.
    function test_by_skt {
        local ARG_PY3=$1
        local ARG_RC=$2

        "${ARG_PY3}" -m skt.executable -vv --state --rc "${ARG_RC}" run --wait || true
    }

    # Run kernel testing using restraint runner.
    function test_by_restraint_runner {
        local ARG_PY3=$1
        local ARG_RC=$2
        local ARG_UPT_OUTPUT=$3
        local ARG_UPT_OUTPUT_DIR=$4

        local yaml_files=("${ARG_UPT_OUTPUT}".*)
        for yaml_file in "${yaml_files[@]}"; do
          (
            "${ARG_PY3}" -m restraint_wrap test --upload --dump --rc "${ARG_RC}" --reruns 1 -i "${yaml_file}" -o "${ARG_UPT_OUTPUT_DIR}" || rc_state_set retcode $?
            upt_release_testing_resources "${ARG_PY3}" "${yaml_file}"
          ) &
        done
        # Wait for testing to complete.
        wait
        # Always cancel every job. May execute on already cancelled job.
        for yaml_file in "${yaml_files[@]}"; do
          upt_release_testing_resources "${ARG_PY3}" "${yaml_file}"
        done
    }

    # Run provisioning using upt.
    function provision_and_test_by_upt {
        local ARG_PY3=$1
        local ARG_RC=$2
        local ARG_EXCLUDE_FILE=$3
        local ARG_UPT_INPUT=$4
        local ARG_UPT_OUTPUT_YAML=$5
        local ARG_UPT_OUTPUT_DIR=$6

        UPT_ARGS=("--time-cap" "4200" "--rc" "${ARG_RC}" "-e" "${ARG_EXCLUDE_FILE}")
        UPT_CONVERT_ARGS=("legacy" "convert" "-i" "${ARG_UPT_INPUT}" "-r" "${ARG_UPT_OUTPUT_YAML}")
        # Convert beaker.xml file to c_req.yaml.
        "${ARG_PY3}" -m upt "${UPT_ARGS[@]}" "${UPT_CONVERT_ARGS[@]}"

        # Run provisioning and testing.
        "${ARG_PY3}" -m upt "${UPT_ARGS[@]}" "provision" "--pipeline" "--reruns 1 --upload --dump -o ${ARG_UPT_OUTPUT_DIR}" -r "${ARG_UPT_OUTPUT_YAML}" || rc_state_set retcode $?
        upt_release_testing_resources "${ARG_PY3}" "${ARG_UPT_OUTPUT_YAML}"
    }

    # Get a list of successfully built selftest sets
    # Args:
    #   $1: path to the selftest result file, each line in the "set_name: exit_code" format
    function get_successful_selftests {
        built_sets=()
        while read -r result ; do
            # Get rid of the extra space at the beginning
            RETCODE=$(echo -e "${result##*:}" | tr -d '[:space:]')
            if [ "${RETCODE}" = "0" ] ; then
                built_sets+=("${result%%:*}")
            fi
        done < "$1"
        # Empty arrays fail as "unbound variable" at Bash < 4.3 (RHEL 7)
        if [ "${#built_sets[@]}" -gt 0  ]; then
            echo TARGETS="${built_sets[*]}"
        else
    	echo "TARGETS="
        fi
    }

    # Extract the kernel config file from SRPM, move it to the artifacts directory and set the
    # correct RC variables. Name of the published config is unified.
    # Args:
    #   $1: path to kernel SRPM
    #   $2: kernel package name
    #   $3: RPM release, dist tag is used if empty string
    function artifact_config_from_srpm {
        rpm2cpio "$1" | cpio -idm -- *.config
        local PACKAGE_NAME=$2
        local RPM_RELEASE=$3

        if is_true "${DEBUG_KERNEL}"; then
            local new_config_suffix="-debug"
        else
            local new_config_suffix=""
        fi
        # Set the new name now, we don't need the RHEL/Fedora suffixes in the name.
        local NEW_CONFIG_NAME="kernel-${ARCH_CONFIG}${new_config_suffix}.config"
        # Fedora and ARK/ELN builds one config for RHEL and one for Fedora and we need to
        # pick the correct one based on either the passed release info from triggers or dist tag.
        if [[ "$(rpm --eval "%{dist}")" =~ \.el || "${RPM_RELEASE}" =~ el ]] ; then
            local suffixes=("${new_config_suffix}" "${new_config_suffix}-rhel")
        else
            local suffixes=("${new_config_suffix}" "${new_config_suffix}-fedora")
        fi
        for suffix in "${suffixes[@]}"; do
            # We have to use the wildcard in the check as we don't know the full file name. RHEL7
            # includes part of the kernel version in the config name while newer kernels don't.
            local config=("${PACKAGE_NAME}"-*"${ARCH_CONFIG}${suffix}.config")
            if [ -f "${config[0]}" ]; then
                mv "${config[0]}" "${ARTIFACTS_DIR}/${NEW_CONFIG_NAME}"
                break
            fi
        done
        if ! [ -f "${ARTIFACTS_DIR}/${NEW_CONFIG_NAME}" ]; then
            # RHEL <= 6 don't have a full config file and it is built up from the various snippets
            # so there's nothing we can upload.
            echo_red "Unable to determine kernel configuration file to upload!"
            echo_yellow "RHEL5 and 6 config file extraction is not supported!"
            return
        fi

        rc_state_set config_file "${ARTIFACTS_DIR}/${NEW_CONFIG_NAME}"
        rc_state_set kernel_config_url "${CI_API_JOB_URL}/artifacts/raw/artifacts/${NEW_CONFIG_NAME}"
        # shellcheck disable=SC2086,SC2153 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086] Yes, the variable name is really ARTIFACT*S* and not ARTIFACT
        rc_set build config_file "$(find $ARTIFACTS -name '*.config' -printf '%p ')"
    }

    # GENERAL-END do not modify this line


    function prepare_main {
      echo_green "📦 Packaging CKI pipeline software."

      # Create all of the required directories.
      mkdir -p "${SOFTWARE_DIR}"

      # Create Python3 venv
      python3 -m venv "${VENV_DIR}"

      chmod -R 777 "${SOFTWARE_DIR}"

      "${VENV_PY3}" -m pip install --upgrade "pip==21.1.*"

      # Download python packages
      for PROJECT in $GITLAB_COM_PACKAGES; do
              ${VENV_PY3} -m pip install "git+https://gitlab.com/cki-project/${PROJECT}.git"
      done


      # CI systems for projects used in the pipeline can pass in their own versions
      # of software that they want to test.
      #
      # For each of the projects in GITLAB_COM_PACKAGES and GITLAB_COM_DATA_PROJECTS,
      # a variable of the following form can be used:
        #
      #  <project>_pip_url = https://url.com/user/project@BRANCH
      #
      # For the variable name, dashes in the project name are replaced by underscores.
      #
      # If this variable is present for a Python package, the currently installed
      # version is uninstalled and the new version is installed instead.
      #
      # If this variable is present for a data project, the currently installed
      # version is deleted and the new version is checked out instead.

      # Override Python packages if necessary.
      for PROJECT in $GITLAB_COM_PACKAGES; do
          override package "$(basename "$PROJECT")"
      done

      # Upload the compressed archive to object storage without touching the disk.
      #echo_green "Uploading packaged software to object storage."
      #loop upload_software_object

      ls -al software/*
      ${VENV_PY3} -m pip list
    }

    # PREPARE-END do not modify this line


        function override {
        local TYPE="$1"
        local PACKAGE_NAME="$2"
        local PACKAGE_NAME_SANITIZED=${PACKAGE_NAME/-/_}
        local PIP_URL="${PACKAGE_NAME_SANITIZED}_pip_url"
        local RESOLVED_PIP_URL="${!PIP_URL:-}"

        if [[ -n "${RESOLVED_PIP_URL}" ]]; then
            echo_yellow "Found ${PACKAGE_NAME} override: ${RESOLVED_PIP_URL}"
            if [ "$TYPE" = "package" ]; then
                ${VENV_PY3} -m pip uninstall -y "${PACKAGE_NAME}"
                ${VENV_PY3} -m pip install "${RESOLVED_PIP_URL}"
            else
                local BRANCH=${RESOLVED_PIP_URL##*@}
                local REPO=${RESOLVED_PIP_URL%@*}
                REPO="$(git_clean_url "${REPO}")"
                rm -rf "${SOFTWARE_DIR:?}/${PACKAGE_NAME}"
                git_clone "${REPO}" "${SOFTWARE_DIR}/${PACKAGE_NAME}"
                (
                  cd "${SOFTWARE_DIR}/${PACKAGE_NAME}"
                  loop git fetch origin "$BRANCH"
                  git checkout FETCH_HEAD
                )
            fi
        fi
    }