#!/bin/bash -x

source helper.sh
source vars.sh


prepare_main


# exit 
  # build-prepare
  # 🛠 Prepare to compile the kernel.
  rc_set build start_time "$(date_utc)"
  # Update the state file with architecture details and package name.
  rc_state_set kernel_arch "${ARCH_CONFIG}"
  rc_state_set package_name "${package_name}"
  if [[ "$(arch)" != "x86_64" ]]; then
    echo "ERROR: Unsupported build machine architecture ($(arch)) for the build stage."
    echo "ERROR: Only 'x86_64' architecture is currently supported as the builder for the build stage"
    exit 1
  fi
  # Save the compiler version
  if [[ "${compiler}" == 'clang' ]] ; then
    # clang does not use cross compile prefixes
    rc_state_set compiler "$(clang --version | head -1)"
  else  # default, no other values supported
    rc_state_set compiler "$("${CROSS_COMPILE:-}"gcc --version | head -1)"
  fi
  # Mark the build stage as failed for now and replace it with a successful
  # status if we make it all the way through the stage later.
  rc_state_set stage_build fail
  rc_state_set buildlog "${BUILDLOG_PATH}"
  # Set the path to the ccache tarball stored in S3.
  # CCACHE_FILENAME="${name}-${CI_JOB_NAME##* }.tar"
  # Create a directory for fixing up kernels
  mkdir -p "${KERNEL_CLEANUP_DIR}"

  # build-prepare-rpm
  # 🛠  Prepare directories to hold binaries and set up macros
  if [[ "${make_target}" = 'rpm' ]]; then
    # Make a directory to hold the RPMs.
    mkdir -p "${WORKDIR}/rpms/${ARCH_CONFIG}/"
    # Set up RPM macros for this build.
    echo "%_rpmdir ${WORKDIR}/rpms" | tee -a ~/.rpmmacros
    # Define path for extra rpmbuild artifacts, like vmlinux.h
    echo "%artifacts ${ARTIFACTS_DIR}/" | tee -a ~/.rpmmacros

    # Override dist tag if specified
    if [ -n "${disttag_override}" ] ; then
      echo_yellow "Manual disttag override requested: ${disttag_override}"
      echo "%dist ${disttag_override}" | tee -a ~/.rpmmacros
    fi
  fi

  ## Restore ccache manually
  echo_yellow "Restoring cache manually from ${CCACHE_FILENAME}"
  tar -xf "${CCACHE_FILENAME}" -C "${ROOT_CCACHE_DIR}"
  mkdir -p "${CCACHE_DIR}"
  cd "${CCACHE_DIR}" || true
  ccache -sz
  cd "${CI_PROJECT_DIR}" || true
  rm "${CCACHE_FILENAME}"

  # build-compile-rpm
  # 📦 Compile the kernel into an RPM.
  # Only do this step if we are building RPMs.
  if [[ $make_target = 'rpm' ]]; then
    # Save the kernel config before building in case there's an error.
    artifact_config_from_srpm "${ARTIFACTS_DIR}"/*.src.rpm "${package_name}" ""

    # Only build the KABI base if we build the stablelist. Some kernels don't support
    # KABI and thus the base fails to build for them as well.
    if is_true "${build_kabi_whitelist}" || is_true "${build_kabi_stablelist}" ; then
      RPMBUILD_WITH="${RPMBUILD_WITH} kabidw_base"
    fi

    # Set up the rpmbuild command arguments and write them to the rc file.
    # Override the defaults for x86_64 as we can build more things here.
    if [[ "$(rc_state_get kernel_arch)" == "x86_64" ]] ; then
        RPMBUILD_WITH=${RPMBUILD_WITH//cross/}
        RPMBUILD_WITHOUT="trace"

        # Enable debug config if we're running the debug build but not on regular x86_64 job!
        if is_true "${DEBUG_KERNEL}"; then
          RPMBUILD_WITH="${RPMBUILD_WITH} debug"
        else
          RPMBUILD_WITHOUT="${RPMBUILD_WITHOUT} debug"
        fi
    fi

    if [[ "${compiler}" == 'clang' ]] ; then
      RPMBUILD_WITH="${RPMBUILD_WITH} toolchain_clang"
    fi

    if is_true "${coverage}"; then
      RPMBUILD_WITH="${RPMBUILD_WITH} gcov"
    fi

    # FIXME Make all RPMBUILD_* variables arrays instead of strings and use either
    # ${RPMBUILD_*[*]} or ${RPMBUILD_*[@]} as appropriate.
    # shellcheck disable=SC2086 # FIXME Disabled on purpose as we want the splits of the words in the variable. It can be removed once join_by_multi uses arrays.
    RPMBUILD_ARGS="--target ${ARCH_CONFIG} $(join_by_multi ' --with ' $RPMBUILD_WITH) $(join_by_multi ' --without ' $RPMBUILD_WITHOUT)"
    # Write our make options to the rc file.
    rc_state_set make_opts "rpmbuild ${RPMBUILD_ARGS}"
    rc_set build command "rpmbuild ${RPMBUILD_ARGS}"
    echo_yellow "Compiling the kernel with: rpmbuild --rebuild ${RPMBUILD_ARGS}"

    # Save timestamp before starting the build
    store_time
    # Build the arch-specific kernel RPMs.
    # shellcheck disable=SC2086 # FIXME Disabled on purpose as we want the spaces in $RPMBUILD_ARGS; can be removed once join_by_multi uses arrays.
    rpmbuild --rebuild $RPMBUILD_ARGS \
      artifacts/*.src.rpm 2>&1 | ts -s | tee -a ${CI_PROJECT_DIR}/${BUILDLOG_PATH} | tail -n 25
    # Build the KABI whitelist RPM if needed.
    if is_true "${build_kabi_whitelist}" || is_true "${build_kabi_stablelist}" ; then
      rpmbuild --rebuild --without doc --target noarch \
        artifacts/*.src.rpm 2>&1 | ts -s | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}" | tail -n 25
    fi
    # Calculate and save build duration after ending the build
    rc_state_set build_time "$(($(calculate_duration) + $(rc_state_get build_time)))"

    # Artifact all RPMs so we can create the repo later
    mv "${WORKDIR}/rpms/"*/*.rpm "${ARTIFACTS_DIR}/"

    rc_state_set stage_build pass
    echo_green "Kernel compilation succeeded. Check the publish stage for full yum/dnf repository."
  fi
