#!/bin/bash

# The purpose of this script is create a kernel build getting the sources
# and creating a new directory to do the build

DESTINATION_FOLDER=$(date '+%Y-%m-%d-%H-%M-%s')

mkdir -p ${DESTINATION_FOLDER}

cp -a main.sh vars.sh helper.sh src/ccache.tar src/artifacts src/rc  ${DESTINATION_FOLDER}


podman run --rm -it \
    --privileged \
    -v ./${DESTINATION_FOLDER}:/builds:Z \
    -w /builds \
    quay.io/cki/builder-rhel8:20210609.1 \
    bash
