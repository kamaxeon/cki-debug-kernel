# cki-debug-kernel

This is an example to try to debug a kernel build locally or in beaker, we use the buildah builder image.

You should put the artifacts, ccache and rc file inside  `src` directory.

After that, you sould run `.create_build.sh`, this script will create a new directory attached to a container and launch a shell inside of the container. To try the compilation, you only need type `./main.sh`.
